import fractions
import pyaudio
import struct
import sys

from common_utils import ColorPrint
from notes_utils import SongBundle
from wave_utils import WaveDataGenerator, WaveFormGenerator


def main(argv):
    if len(argv) != 1:
        ColorPrint.color_print('fail', 'Usage: player.py <filename>')
    # Read Song
    song_bundle = SongBundle(argv[0])
    sample_rate = 48000
    frame_depth = 2
    wave_data_gen = WaveDataGenerator(sample_rate=sample_rate)
    for note in song_bundle.get_notes:
        note_len = note[0].split('/')
        if len(note_len) == 1:
            note_len = fractions.Fraction(int(note_len[0]), 1)
        else:
            note_len = fractions.Fraction(int(note_len[0]), int(note_len[1]))
        pitch = note[1]
        wave_data_gen.append_wave_form(WaveFormGenerator.sine_wave, pitch, note_len, song_bundle.get_tempo)
    wave_data = ''
    for frame in wave_data_gen.get_wave_data:
        wave_data += struct.pack('h', frame)
    pyaudio_instance = pyaudio.PyAudio()
    stream = pyaudio_instance.open(format=pyaudio_instance.get_format_from_width(frame_depth),
                                   channels=1, rate=sample_rate, output=True)

    wave_data_size = len(wave_data)
    wave_data_chunk_size = 1024
    wave_data_chuck_count = wave_data_size / wave_data_chunk_size
    for wave_data_chunk_idx in range(0, wave_data_chuck_count):
        wave_data_chunk = wave_data[wave_data_chunk_size * wave_data_chunk_idx:
                                    wave_data_chunk_size * (wave_data_chunk_idx + 1)]
        stream.write(wave_data_chunk)
    if (wave_data_size % wave_data_chunk_size) != 0:
        stream.write(wave_data[wave_data_chuck_count * wave_data_chuck_count:])

    # stop stream
    stream.stop_stream()
    stream.close()

    # close PyAudio
    pyaudio_instance.terminate()
    return 0


sys.exit(main(sys.argv[1:]))
