import argparse
import sys

from pitch_processor_impl import PitchProcessor
from audio_listener_impl import AudioListener
from common_utils import ColorPrint


def get_args(argv):
    parser = argparse.ArgumentParser(description='musefinder: ARGUMENTS')
    parser.add_argument('-v', '--verbose', help='Verbose output', type=bool, default=False)
    parser.add_argument('-i', '--input-type', help='Input type', type=str, default='device', choices=['wave', 'device'])
    parser.add_argument('-s', '--songs-dir', help='Directory to song definitions', type=str, default='./songs/')
    parser.add_argument('-w', '--window', help='Consecutive window distance in seconds', type=float, default=0.01)
    parser.add_argument('-o', '--overlap', help='Window overlap percentage', type=float, default=0.5)
    parser.add_argument('-f', '--file', help='Input File (provide in case that source is selected as wave)', type=str)
    parser.add_argument('--tempo-range', help='Input relative tempo range', nargs=2, type=float, default=[0.75, 1.5])
    parser.add_argument('--min-magnitude', help='Minimum detectable note magnitude (dB)', type=float, default=10.0)
    parser.add_argument('--min-snr', help='Minimum acceptable note SNR (dB)', type=float, default=0.0)
    parser.add_argument('--min-score', help='Minimum score for the note to be acceptable', type=float, default=1.9)
    parser.add_argument('--false-note-score-reduction', help='Impact of false notes on score per 1/2 step', type=float,
                        default=0.5)
    parser.add_argument('--tempo-wiggle-score-reduction', help='Score reduction for tempo wiggle', type=float,
                        default=0.05)
    parser.add_argument('--sampling-freq', help='Recording device sampling rate', type=int, default=48000)
    parser.add_argument('--sample_width', help='Sample width in bytes', type=int, default=2)
    args = parser.parse_args(argv)
    if args.input_type == 'wave':
        if args.file is None:
            sys.exit('--source is selected as "wave" but there is no --file argument')
        ColorPrint.color_print('warning', 'if --input-type="wave", --sampling-freq and --sampling-width'
                                          'settings are ignored and interpreted from the wave file')
    return args


def main(argv):
    args = get_args(argv)
    if args.input_type == 'device':
        audio_listener = AudioListener(frame_rate=args.sampling_freq, frame_width=args.sample_width,
                                       frames_per_buffer=1024)
    else:
        audio_listener = AudioListener(wave_file_address=args.file, frames_per_buffer=1024)
    audio_processor = PitchProcessor(audio_listener, 0, args.window, args.overlap)
    try:
        audio_processor.start()
        audio_listener.start()
        while not (audio_listener.join(0.1) and audio_processor.join(0.1)):
            pass
    except KeyboardInterrupt:
        print 'Exiting MuseFinder ...'
        audio_listener.stop(0)
        while not (audio_listener.join(0.1) and audio_processor.join(0.1)):
            pass
        sys.exit(0)
    return 0


sys.exit(main(sys.argv[1:]))
