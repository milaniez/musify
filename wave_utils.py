import math
import struct
import wave

from notes_utils import PitchFreqUtils


class WaveFormGenerator:
    def __init__(self):
        pass

    @staticmethod
    def sawtooth(x):
        x = float(x)
        return 2.0*((x % 1.0) - 0.5)

    @staticmethod
    def sine_wave(freq, length, width=2, volume=1.0):
        if freq == 0.0:
            return [0] * length
        freq = float(freq)
        max_val = float(2 ** (width*8 - 1) - 1) * volume
        return [int(math.sin(2.0*math.pi*freq*float(i))*max_val) for i in range(0, length)]

    @staticmethod
    def sawtooth_wave(freq, length, width=2, volume=1.0):
        if freq == 0.0:
            return [0] * length
        freq = float(freq)
        max_val = float(2 ** (width*8 - 1) - 1) * volume
        return [int(WaveFormGenerator.sawtooth(freq*float(i))*max_val) for i in range(0, length)]


class WaveDataGenerator:
    def __init__(self, sample_rate, width=2, volume=1.0):
        self.sample_rate = sample_rate
        self.width = width
        self.volume = volume
        self.wave_data = []
        self.total_length = 0.0

    def append_wave_form(self, wave_generator, freq_or_pitch, duration, tempo=None):
        if tempo is not None:
            duration = float(duration) * 4.0 * 60.0 / float(tempo)
        if type(freq_or_pitch) is str:
            freq = PitchFreqUtils.get_freq(freq_or_pitch)
        else:
            freq = freq_or_pitch
        self.total_length += duration
        prev_size = len(self.wave_data)
        total_size = int(float(self.sample_rate)*self.total_length)
        size_diff = total_size - prev_size
        self.wave_data.extend(wave_generator(float(freq) / float(self.sample_rate), size_diff, width=self.width))

    @property
    def get_wave_data(self):
        return self.wave_data

    def save_wav(self, file_address):
        packed_data = ''
        for frame in self.wave_data:
            packed_data += struct.pack('h', frame)
        wave_file = wave.open(file_address, 'w')
        wave_file.setframerate(self.sample_rate)
        wave_file.setnchannels(1)
        wave_file.setsampwidth(self.width)
        wave_file.writeframes(packed_data)
        wave_file.close()
