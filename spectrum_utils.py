import collections
import numpy

from common_utils import MathUtils


class Periodogram:
    peak_type = collections.namedtuple('peak_type', ['range_lower', 'range_higher', 'frequency', 'magnitude'])

    def __init__(self, time_series):
        self.time_series = time_series
        fft_result = numpy.absolute(numpy.fft.fft(time_series))
        self.fft_orig_len = len(fft_result)
        self.freq_step = 1.0 / float(self.fft_orig_len)
        self.freq_vals = numpy.array(range(0, self.fft_orig_len / 2 + 1)) / float(self.fft_orig_len)
        self.periodogram_len = len(self.freq_vals)
        self.fft_result = fft_result[0:self.periodogram_len]
        self.fft_result[0] = 0.0
        if (self.fft_orig_len % 2) == 0:
            self.fft_result[-1] /= 2.0
        self.periodogram = self.fft_result ** 2
        # noinspection PyTypeChecker
        self.norm_periodogram = self.periodogram / sum(self.periodogram) * self.periodogram_len
        self.peak_hints = None
        self.autocorrelation = None

    @property
    def get_norm_periodogram(self, freq=None):
        if freq is None:
            return self.norm_periodogram
        else:
            if (freq >= 1) or (freq < 0):
                raise ValueError('freq value out of bound')
            if freq > 0.5:
                freq = 1 - freq
            idx1 = int(numpy.floor(freq / self.freq_step))
            idx2 = idx1 + 1
            if idx2 >= self.periodogram_len:
                # noinspection PyUnresolvedReferences
                return self.norm_periodogram[-1]
            # noinspection PyUnresolvedReferences
            m1 = self.norm_periodogram[idx1]
            # noinspection PyUnresolvedReferences
            m2 = self.norm_periodogram[idx2]
            f1 = float(idx1) * self.freq_step
            f2 = float(idx2) * self.freq_step
            return ((freq - f1) * m2 + (f2 - freq) * m1) / self.freq_step

    def get_highest_peak(self, threshold):
        threshold *= self.periodogram_len
        # noinspection PyTypeChecker
        max_idx = numpy.argmax(self.norm_periodogram)
        if max_idx > numpy.sqrt(self.fft_orig_len):
            return float(max_idx)*self.freq_step
        

    def get_peak_hints(self, threshold):
        threshold *= self.periodogram_len
        compare_results = numpy.where(numpy.diff(numpy.greater(self.norm_periodogram, threshold)))[0]
        peak_hints = []
        for idx_idx in range(0, len(compare_results), 2):
            idx1 = compare_results[idx_idx] + 1
            f1 = idx1 * self.freq_step
            if idx_idx < len(compare_results) - 1:
                idx2 = compare_results[idx_idx + 1]
                f2 = idx2 * self.freq_step
                f = numpy.average(self.freq_vals[idx1:(idx2 + 1)], weights=self.norm_periodogram[idx1:(idx2 + 1)])
                m = numpy.sum(self.norm_periodogram[idx1:(idx2 + 1)]) * self.freq_step
            else:
                f2 = 1 - f1
                f = 0.5
                m = numpy.sum(self.norm_periodogram[idx1:]) * self.freq_step
            peak_hints.append(Periodogram.peak_type(f1, f2, f, m))
        self.peak_hints = peak_hints
        return peak_hints

    def get_peaks(self, threshold):
        if self.peak_hints is None:
            self.get_peak_hints(threshold)
        if self.autocorrelation is None:
            from numpy.fft import fft, ifft
            # noinspection PyTypeChecker
            self.autocorrelation = numpy.abs(ifft(fft(self.time_series) * fft(self.time_series).conj()))
        for peak_hint in self.peak_hints:
            period_hint = 1.0 / peak_hint.frequency
            period_range_lo = numpy.floor(1.0 / peak_hint.range_higher)
            period_range_hi = numpy.ceil(1.0 / peak_hint.range_lower) + 1
            period = period_range_lo + self.autocorrelation[period_range_lo:period_range_hi].argmax()
            is_hill = MathUtils.is_hill(self.autocorrelation[period_range_lo:period_range_hi],
                                        int(numpy.round(period_hint) - period_range_lo))
            pass
        return None


