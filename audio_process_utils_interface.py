from abc import ABCMeta, abstractmethod, abstractproperty


class IMultiOutputThreadHandler(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def stop(self, idx):
        raise NotImplementedError('method not implemented')

    @abstractmethod
    def get_queue(self, idx):
        raise NotImplementedError('method not implemented')

    @abstractmethod
    def get_stop_event(self, idx):
        raise NotImplementedError('method not implemented')


class IAudioListener(object):

    __metaclass__ = ABCMeta

    @abstractproperty
    def get_frame_rate(self):
        raise NotImplementedError('property not implemented')

    @abstractproperty
    def get_frame_width(self):
        raise NotImplementedError('property not implemented')

    @abstractproperty
    def get_frames_per_buffer(self):
        raise NotImplementedError('property not implemented')


class IAudioToPitchConverted(object):

    __metaclass__ = ABCMeta

    @abstractproperty
    def get_window_distance(self):
        raise NotImplementedError('property not implemented')

    @abstractproperty
    def get_window_overlap(self):
        raise NotImplementedError('property not implemented')