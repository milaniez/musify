import fractions
import json
import re

import pyparsing

from common_utils import MathUtils


class PitchFreqUtils:
    pitch_freqs = {'A4': 440.0, 'M0': 0.0}
    relative_pitch_freq = {'C': 2.0 ** (-9.0 / 12.0), 'C#': 2.0 ** (-8.0 / 12.0),
                           'Db': 2.0 ** (-8.0 / 12.0), 'D': 2.0 ** (-7.0 / 12.0), 'D#': 2.0 ** (-6.0 / 12.0),
                           'Eb': 2.0 ** (-6.0 / 12.0), 'E': 2.0 ** (-5.0 / 12.0),
                           'F': 2.0 ** (-4.0 / 12.0), 'F#': 2.0 ** (-3.0 / 12.0),
                           'Gb': 2.0 ** (-3.0 / 12.0), 'G': 2.0 ** (-2.0 / 12.0), 'G#': 2.0 ** (-1.0 / 12.0),
                           'Ab': 2.0 ** (-1.0 / 12.0), 'A': 2.0 ** (0.0 / 12.0), 'A#': 2.0 ** (1.0 / 12.0),
                           'Bb': 2.0 ** (1.0 / 12.0), 'B': 2.0 ** (2.0 / 12.0),
                           'M': 0.0}
    pitch_split_prog = re.compile('^([A-GM][#b]?)([0-8])$')

    def __init__(self):
        pass

    @staticmethod
    def is_valid_pitch(pitch):
        matches = PitchFreqUtils.pitch_split_prog.match(pitch)
        if not matches:
            return False
        pitch_char = matches.group(1)
        octave = int(matches.group(2))
        if (octave >= 0) and (octave <= 8) and (pitch_char in PitchFreqUtils.relative_pitch_freq):
            return True
        return False

    @staticmethod
    def _get_relative_freq_4(pitch_char):
        return PitchFreqUtils.relative_pitch_freq[pitch_char] * PitchFreqUtils.pitch_freqs['A4']

    @staticmethod
    def _get_octave_freq(pitch):
        matches = PitchFreqUtils.pitch_split_prog.match(pitch)
        pitch_char = matches.group(1)
        octave = int(matches.group(2))
        pitch4 = pitch_char + '4'
        if pitch4 not in PitchFreqUtils.pitch_freqs:
            freq = PitchFreqUtils._get_relative_freq_4(pitch_char)
            PitchFreqUtils.pitch_freqs[pitch4] = freq
        return PitchFreqUtils.pitch_freqs[pitch4] * (2.0 ** (float(octave - 4)))

    @staticmethod
    def get_freq(pitch):
        if pitch not in PitchFreqUtils.pitch_freqs:
            freq = PitchFreqUtils._get_octave_freq(pitch)
            PitchFreqUtils.pitch_freqs[pitch] = freq
        return PitchFreqUtils.pitch_freqs[pitch]


class NoteExpander:
    def __init__(self, notes):
        if (not notes.startswith('{')) or (not notes.endswith('}')):
            notes = '{1:' + notes + '}'
        notes_parser = pyparsing.nestedExpr('{', '}')
        self.parsed_notes = notes_parser.parseString(notes).asList()[0]
        count_finder_pattern = '^([0-9])+:(.*)$'
        self.prog = re.compile(count_finder_pattern, re.DOTALL | re.MULTILINE)

    def _expand_str(self, notes):
        matches = self.prog.match(notes)
        repeat_count = int(matches.group(1))
        note_sequence = matches.group(2)
        retval = ''
        for _unused in range(0, repeat_count):
            retval += note_sequence
        return retval

    def _expand(self, notes):
        notes_expanded = ''
        for note in notes:
            if type(note) is str:
                notes_expanded += note
            else:
                notes_expanded += self._expand(note)
        return self._expand_str(notes_expanded)

    def expand(self):
        return self._expand(self.parsed_notes)


class SongBundle:
    def __init__(self, file_name):
        with open(file_name, 'r') as song_file:
            data = json.load(song_file)
        self.artist = data['Artist'].encode('ascii', 'ignore') if 'Artist' in data else 'Unknown'
        self.title = data['Title'].encode('ascii', 'ignore') if 'Title' in data else 'Unknown'
        self.album = data['Album'].encode('ascii', 'ignore') if 'Album' in data else 'Unknown'
        self.tempo = data['Tempo'] if 'Tempo' in data else 120
        self.notes = ''.join(re.split('\s*\|+\s*|\s+', u''.join(data['Notes']).encode('ascii', 'ignore')))
        notes_expander = NoteExpander(self.notes)
        self.expanded_notes = notes_expander.expand()
        self.separated_notes = [i.split(':') for i in self.expanded_notes.strip('()').split(')(')]
        self.note_duration_noms = []
        self.note_duration_dens = []
        for note in self.separated_notes:
            duration_str = note[0]
            if '/' in duration_str:
                self.note_duration_noms.append(int(duration_str.split('/')[0]))
                self.note_duration_dens.append(int(duration_str.split('/')[1]))
            else:
                self.note_duration_noms.append(int(duration_str))
                self.note_duration_dens.append(1)

    @property
    def get_notes(self):
        return self.separated_notes

    @property
    def get_tempo(self):
        return self.tempo

    @property
    def get_beat_count(self):
        note_duration_den_set = list(set(self.note_duration_dens))
        notes_den_lcm = MathUtils.lcm(note_duration_den_set)
        return fractions.Fraction(sum([(self.note_duration_noms[i] * notes_den_lcm) / self.note_duration_dens[i]
                                       for i in range(0, len(self.separated_notes))]), notes_den_lcm)
