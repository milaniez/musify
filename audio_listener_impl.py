import Queue
import array
import pyaudio
import struct
import sys
import threading
import wave

from audio_process_utils_interface import IAudioListener, IMultiOutputThreadHandler


class AudioListener(threading.Thread, IAudioListener, IMultiOutputThreadHandler):

    def __init__(self, wave_file_address=None, frame_rate=None, frame_width=None,
                 frames_per_buffer=None, processor_count=1):
        super(AudioListener, self).__init__()
        if wave_file_address is not None:
            if frames_per_buffer is None:
                sys.exit('input type is wave but frames_per_buffer is not configured')
            self.input_type = 'wave'
            self.wave_file = wave.open(wave_file_address, 'r')
            self.frame_rate = self.wave_file.getframerate()
            self.frame_width = self.wave_file.getsampwidth()
            self.frames_per_buffer = frames_per_buffer
        else:
            if (frame_rate is None) or (frame_width is None) or (frames_per_buffer is None):
                sys.exit('input type is stream but stream configurations is not set')
            self.input_type = 'device'
            self.frame_rate = frame_rate
            self.frame_width = frame_width
            self.frames_per_buffer = frames_per_buffer
            self.device_stream = pyaudio.PyAudio().open(
                format=[None, pyaudio.paInt8, pyaudio.paInt16, pyaudio.paInt24, pyaudio.paInt32][frame_width],
                channels=1, rate=frame_rate, frames_per_buffer=frames_per_buffer, input=True)
        self.queues = []
        self.stop_events = []
        self.process_count = processor_count
        self.active_process_count = processor_count
        self.active_processes = [True] * processor_count
        if self.frame_width == 1:
            self.frame_format = 'b'
        elif self.frame_width == 2:
            self.frame_format = 'h'
        elif self.frame_width == 4:
            self.frame_format = 'i'
        else:
            raise ValueError('frame width is not supported')
        for i in range(0, processor_count):
            self.queues.append(Queue.Queue(16))
            self.stop_events.append(threading.Event())

    def __del__(self):
        if self.input_type == 'wave':
            self.wave_file.close()
        else:
            self.device_stream.close()

    def run(self):
        try:
            if self.input_type == 'wave':
                frame_count = self.wave_file.getnframes()
                chunk_count = frame_count / self.frames_per_buffer
                if frame_count % self.frames_per_buffer:
                    chunk_count += 1
                for chuck_idx in range(0, chunk_count):
                    if self.active_process_count == 0:
                        return
                    data = self.wave_file.readframes(self.frames_per_buffer)
                    unpacked_data = array.array(self.frame_format,
                                                list(struct.unpack(str(len(data) / 2) + self.frame_format, data)))
                    for idx in range(0, self.process_count):
                        if self.stop_events[idx].wait(timeout=0):
                            self.active_processes[idx] = False
                            continue
                        if self.active_processes[idx]:
                            self.queues[idx].put(unpacked_data)
            else:
                while True:
                    if self.active_process_count == 0:
                        return
                    stream_data = self.device_stream.read(self.frames_per_buffer)
                    for idx in range(0, self.process_count):
                        if self.stop_events[idx].wait(timeout=0):
                            self.active_processes[idx] = False
                            self.active_process_count -= 1
                            continue
                        if self.active_processes[idx]:
                            unpacked_data = list(struct.unpack(str(self.frames_per_buffer) + self.frame_format,
                                                               stream_data))
                            self.queues[idx].put(array.array(self.frame_format, unpacked_data))
        finally:
            for idx in range(0, self.process_count):
                if self.active_processes[idx]:
                    self.stop_events[idx].set()

    def stop(self, idx=None):
        if idx is None:
            for stop_event in self.stop_events:
                stop_event.set()
        else:
            if self.active_processes[idx]:
                self.stop_events[idx].set()

    @property
    def get_frame_rate(self):
        return self.frame_rate

    @property
    def get_frame_width(self):
        return self.frame_width

    @property
    def get_frames_per_buffer(self):
        return self.frames_per_buffer

    def get_queue(self, idx):
        return self.queues[idx]

    def get_stop_event(self, idx):
        return self.stop_events[idx]

    def join(self, timeout=None):
        if self.is_alive():
            super(AudioListener, self).join(timeout=timeout)
        if not self.is_alive():
            if self.input_type == 'wave':
                self.wave_file.close()
            else:
                self.device_stream.close()
            return True
        return False