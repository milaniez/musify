import fractions

import numpy


class ColorPrint:
    END_CODE = '\033[0m'

    FORMATS = {'header': '\033[95m', 'ok_blue': '\033[94m', 'ok_green': '\033[92m', 'warning': '\033[93m',
               'fail': '\033[91m', 'bold': '\033[1m', 'underline': '\033[4m'}

    def __init__(self):
        pass

    @staticmethod
    def color_print(print_format, msg):
        print (ColorPrint.FORMATS[str(print_format).lower()] + msg + ColorPrint.END_CODE)


class MathUtils:
    def __init__(self):
        pass

    @staticmethod
    def gcd(numbers):
        retval = numbers[0]
        for i in range(1, len(numbers)):
            retval = fractions.gcd(retval, numbers[i])
        return retval

    @staticmethod
    def lcm(numbers):
        retval = numbers[0]
        for i in range(1, len(numbers)):
            retval = retval * numbers[i] / fractions.gcd(retval, numbers[i])
        return retval

    @staticmethod
    def is_hill(array, idx):
        if len(array) <= (idx - 2):
            raise ValueError('idx must be in range of array length')
        center_val = array[idx]
        left_slop = numpy.average(1.0 / numpy.array(range(idx, 0, -1)) * (array[0:idx] - center_val))
        right_slop = -numpy.average(1.0 / numpy.array(range(1, len(array) - idx)) * (array[idx + 1:] - center_val))
        if right_slop >= left_slop:
            return True
        return False
        pass


class StatUtils:
    def __init__(self):
        pass

    @staticmethod
    def wighted_var(d, w):
        pass
