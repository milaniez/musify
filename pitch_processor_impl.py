import Queue
import array
import threading

from spectrum_utils import Periodogram


class PitchProcessor(threading.Thread):

    def __init__(self, audio_listener, idx, window_distance=0.01, window_overlap=0.5):
        super(PitchProcessor, self).__init__()
        self.queue = audio_listener.get_queue(idx)
        self.stop_event = audio_listener.get_stop_event(idx)
        self.frame_width = audio_listener.get_frame_width
        self.frame_rate = audio_listener.get_frame_rate
        self.frames_per_buffer = audio_listener.get_frames_per_buffer
        self.window_distance = int(round(window_distance * self.frame_rate))
        self.window_overlap = window_overlap
        self.window_len = int(round(1.0 / (1.0 - window_overlap) * self.window_distance))
        if self.frame_width == 1:
            self.frame_format = 'b'
        elif self.frame_width == 2:
            self.frame_format = 'h'
        elif self.frame_width == 4:
            self.frame_format = 'i'
        else:
            raise ValueError('frame width is not supported')
        self.frames = array.array(self.frame_format)
        self.windows = array.array('f')

    def run(self):
        while True:
            if self.stop_event.wait(timeout=0.0) and self.queue.empty():
                break
            data = None
            try:
                data = self.queue.get(timeout=0.1)
            except Queue.Empty:
                pass
            if data is not None:
                print 'Process Data here ...'
                self.frames.extend(data)
                while len(self.frames) >= self.window_len:
                    window = self.frames[0:self.window_len]
                    self.frames = self.frames[self.window_distance:]
                    window_periodogram = Periodogram(window)
                    significant_peaks = window_periodogram.get_peaks(0.01)
                    pass

    def join(self, timeout=None):
        if self.is_alive():
            super(PitchProcessor, self).join(timeout=timeout)
        return not self.is_alive()