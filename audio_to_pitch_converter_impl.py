import Queue
import array
import threading

from audio_process_utils_interface import IAudioToPitchConverted, IMultiOutputThreadHandler


class AudioToPitchConverter(threading.Thread, IAudioToPitchConverted, IMultiOutputThreadHandler):

    def __init__(self, audio_listener, window_distance=0.01, window_overlap=0.5, pitch_processor_count=1):
        super(AudioToPitchConverter, self).__init__()
        self.input_queue = audio_listener.get_queue  # type: Queue.Queue
        self.input_stop_event = audio_listener.get_stop_event  # type: threading.Event
        self.frame_width = audio_listener.get_frame_width  # type: int
        self.frame_rate = audio_listener.get_frame_rate  # type: int
        self.frames_per_buffer = audio_listener.get_frames_per_buffer  # type: int
        self.window_distance = int(round(window_distance * self.frame_rate))  # type: int
        self.window_overlap = window_overlap  # type: float
        self.window_len = int(round(1.0 / (1.0 - window_overlap) * self.window_distance))  # type: int
        if self.frame_width == 1:
            self.frame_format = 'b'
        elif self.frame_width == 2:
            self.frame_format = 'h'
        elif self.frame_width == 4:
            self.frame_format = 'i'
        else:
            raise ValueError('frame width is not supported')
        self.frames = array.array(self.frame_format)
        self.windows = array.array('f')
        self.output_queues = []  # type: List[Queue.Queue]
        self.output_stop_events = []
        self.process_count = pitch_processor_count
        self.active_process_count = pitch_processor_count
        self.active_processes = [True] * pitch_processor_count
        for i in range(0, pitch_processor_count):
            self.output_queues.append(Queue.Queue(16))
            self.output_stop_events.append(threading.Event())
        pass

    def stop(self, idx=None):
        if idx is None:
            for stop_event in self.output_stop_events:
                stop_event.set()
            self.input_stop_event.set()
        else:
            if self.active_processes[idx]:
                self.output_stop_events[idx].set()

    def run(self):
        try:
            if (self.active_process_count == 0) and self.input_queue.empty():
                return
            while True:
                if (self.active_process_count == 0) and self.input_queue.empty():
                    return
                data = None
                try:
                    data = self.input_queue.get(timeout=0.1)
                except Queue.Empty:
                    pass
                unpacked_data = array.array(self.frame_format,
                                            list(struct.unpack(str(len(data) / 2) + self.frame_format, data)))
                for idx in range(0, self.process_count):
                    if self.stop_events[idx].wait(timeout=0):
                        self.active_processes[idx] = False
                        continue
                    if self.active_processes[idx]:
                        self.queues[idx].put(unpacked_data)
        finally:
            for idx in range(0, self.process_count):
                if self.active_processes[idx]:
                    self.stop_events[idx].set()

    @property
    def get_window_distance(self):
        return self.window_distance

    @property
    def get_window_overlap(self):
        return self.window_overlap

    def get_stop_event(self, idx):
        return self.output_stop_events[idx]

    def get_queue(self, idx):
        return self.output_queues[idx]